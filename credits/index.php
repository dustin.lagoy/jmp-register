<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>JMP: Website Credits</title>

		<link rel="stylesheet" type="text/css" href="../style.css" />
	</head>

	<body>
		<h1><img src="../static/jmp_beta.png" alt="JMP" /></h1>
		<h1>Website Credits</h1>

		<ul>
			<li>Website and registration system by <a href="https://ossguy.com/">Denver Gingerich</a> and others.  jmp-register is licensed under AGPLv3+. You can download the Complete Corresponding Source code <a href="https://gitlab.com/ossguy/jmp-register">here</a>.</li>
			<li>Backend and other code available from the <a href="https://soprani.ca">Soprani.ca</a> family of projects.</li>
			<li><a href="../static/numbers.svg">numbers.svg</a> created by <a href="https://singpolyma.net">Stephen Paul Weber</a> based on work by <a href="https://thenounproject.com/term/phone-number/3377914/">LAFS</a>, CC-BY</a></li>
			<li><a href="../static/share.svg">share.svg</a> created by <a href="https://thenounproject.com/term/share/3147017/">Justin Blake</a>, CC-BY</a></li>
			<li><a href="../static/devices.svg">devices.svg</a> created by <a href="https://thenounproject.com/term/devices/127450/">Funtastic</a>, CC-BY</a></li>
			<li><a href="../static/getjid.svg">getjid.svg</a> and <a href="../static/havejid.svg">havejid.svg</a> created by <a href="https://singpolyma.net">Stephen Paul Weber</a> based on <a href="https://en.wikipedia.org/wiki/File:Jabber_logo.svg">the Jabber logo</a> and icons by <a href="https://thenounproject.com/term/plus/569907/">Kevin</a> and <a href="https://thenounproject.com/term/checkmark/569906/">Kevin</a>, CC-BY</li>
			<li>Free Software logo created by <a href="https://free-software-logo.codeberg.page/">mray</a>, CC0</li>
		</ul>

		<?php require dirname(__FILE__).'/../nav.php'; ?>
	</body>
</html>
